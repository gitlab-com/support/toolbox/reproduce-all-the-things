=begin
Destroy old pipelines via Rails Console

Add cutoff time to from_time.

Use case:
When you have a very large history of pipelines that needs pruning.
The DestroyPipelineService **should** destroy all related objects and database relations, but there may be issues destroying artifacts from disk.

=end

from_time = 1.year.ago
current_user = User.find(1)
pipelines = Ci::Pipeline.where('finished_at < ?', from_time)

pipelines.find_each do |pipeline|
	project = pipeline.project
	result = ::Ci::DestroyPipelineService.new(project, current_user).execute(pipeline)
	puts result.success?
end