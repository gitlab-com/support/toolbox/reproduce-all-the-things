#### Extract push event SHAs from database for a given project

# [Zendesk #254789 (Internal Only)](https://gitlab.zendesk.com/agent/tickets/254789)

# Get the project in question (project ID 9999 in this example)
p = Project.find(9999)

# Print the push event history for the master branch (commit_from, commit_to, created_at)
p.events.where(action: 'pushed').find_each(batch_size: 100) do |e|
    if e.push_event_payload[:ref] == "master" then puts "#{e.push_event_payload[:commit_from]} #{e.push_event_payload[:commit_to]} #{e.created_at}" end
end
