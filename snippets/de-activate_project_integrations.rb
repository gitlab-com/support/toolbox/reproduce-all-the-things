=begin
Deactivate or Activate project integrations

You can specify whether to deactivate or activate matching integrations, as well as filter by integration types, and project ids.

Use case:
When a customer needs to Deactivate or Activate a large amount of project integrations.

=end

# Specifies the active status we are searching for
# true for active, false for inactive
search_status = true

# Specifies the active status we will set for each integration returned
# true for active, false for inactive
set_status = false

# OPTIONAL: Return all available integration types. Helps in determining what integration types you want to disable
# Uncomment line below
#pp Integration.available_integration_types

# Add appropriate integration types to array
filter_integration_types = ["SlackService","JiraService","DiscordService"]

# OR, if you want to disable ALL types
# Uncomment the line below
#filter_integration_types = Integration.available_integration_types

# Get active integrations
active_integrations = Integration.where(active: search_status).where(type: filter_integration_types)

# OR, if you want to restrict by project ids
# Uncomment lines below
#projects = [1,2,3]
#active_integrations.where(project_id: projects)

# Deactivate integrations in batches
active_integrations.find_each do |integration|
    integration.active = set_status
    integration.save!
end
