#!/bin/bash

Help()
{
   # Display Help
   echo "Deploy GitLab on Docker Script"
   echo "Created by @benjaminking"
   echo
   echo "Syntax: docker-deploy.sh [-h|-v {version} |-u {URL}]"
   echo "options:"
   echo "h     Print this help."
   echo "v     GitLab version. Example: '14.7.0-ee.0'"
   echo "u     External URL. Example: 'gitlab.example.com'"
   echo
   echo "Example: docker-deploy.sh -v 14.7.0-ee.0 -u gitlab.example.com"
}

# getopts check
while getopts hv:u: flag
do
case "${flag}" in
                h)
                   Help
                   exit;;
                v) version=${OPTARG}
                        ;;
                u) url=${OPTARG}
                         ;;
                *) echo "Invalid option: -$flag" ;;
        esac
done

# If both -v and -u weren't provided, through the error. 
if [[ ! "${version}" || ! "${url}" ]]; then
  echo "You must provide the version and external URL. Use -h for an example."
  exit 1
fi

 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
 echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update -y
apt-get install docker-ce docker-ce-cli containerd.io -y
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
cat <<EOF > docker-compose.yml
version: "3"
services:
  gitlab:
    image: 'gitlab/gitlab-ee:${version}'
    container_name: 'gitlab-${version}'
    restart: always
    hostname: '${url}'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'https://${url}'
    ports:
      - '80:80'
      - '443:443'
      - '2222:22'
EOF
docker-compose up -d
# Detect if GitLab is version 14 or greater. If so, proceed to wait for the initial root password
MAJOR=`echo ${version} | cut -d. -f1`
if [ $MAJOR -ge "14" ]
then
    echo "***************************************************"
    echo "GitLab version 14 (or greater) detected! Waiting for the root password"
    echo "***************************************************"
    # Loop to wait for the root password, then print it to the console
    while :
    do
    # Check if the file exists
    if docker exec -it gitlab-${version} stat /etc/gitlab/initial_root_password &> /dev/null
    then
        # Output the password once it exists
        echo "Find the root password below."
        docker exec -it gitlab-${version} grep 'Password:' /etc/gitlab/initial_root_password
        break
    fi
    # If the file doesn't exist, sleep 5 seconds before checking again
    sleep 5
    done
fi
echo "***************************************************"
echo "The GitLab container is now starting. This may take a few minutes."
echo "Navigate to https://${url} to access it."
echo "Use docker logs -f gitlab-${version} to check on the startup process."
echo "***************************************************"